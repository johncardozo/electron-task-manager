const { app, BrowserWindow, Menu, ipcMain } = require("electron");
const url = require("url");
const path = require("path");

const { crearVentanaNuevaTarea } = require("./templates/nuevaTarea");

// Electron reload en modo desarrollo
if (process.env.NODE_ENV !== "production") {
  require("electron-reload")(__dirname, {});
}

let ventanaPrincipal;
let ventanaNuevaTarea;

// Ready => Aplicación está lista
app.on("ready", () => {
  // Crea la ventana principal
  ventanaPrincipal = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true,
    },
  });
  // Crea la url de la ventana
  let urlVentana = url.format({
    pathname: path.join(__dirname, "views/index.html"),
    protocol: "file",
    slashes: true,
  });

  // Carga el menu de un template
  const menuPrincipal = Menu.buildFromTemplate(templateMenu);

  // Asigna el menú creado a la aplicación
  Menu.setApplicationMenu(menuPrincipal);

  // Crea la ventana principal cargando un HTML
  ventanaPrincipal.loadURL(urlVentana);

  // Cuando se cierre la ventana principal, se cierra al aplicación
  ventanaPrincipal.on("closed", () => {
    app.quit();
  });

  // Escucha el evento para la cración de una nueva tarea
  ipcMain.on("nueva-tarea", (evento, datos) => {
    // Enviar el mensaje a la ventana principal
    ventanaPrincipal.webContents.send("nueva-tarea", datos);
    // Cierra la ventana
    ventanaNuevaTarea.close();
  });
});

const templateMenu = [
  {
    label: "Tareas",
    submenu: [
      {
        label: "Nueva tarea",
        accelerator: "Ctrl+N",
        click() {
          // Crear la ventana de crear tarea
          ventanaNuevaTarea = crearVentanaNuevaTarea();
        },
      },
      {
        label: "Salir",
        accelerator: process.platform === "darwin" ? "command+Q" : "Ctrl+Q",
        click() {
          app.quit();
        },
      },
    ],
  },
];

// Vefifica si el sistema operativo es Mac os
if (process.platform === "darwin") {
  templateMenu.unshift({ label: app.getName() });
}

// Activa/desactiva el Dev Tools
if (process.env.NODE_ENV !== "production") {
  templateMenu.push({
    label: "DevTools",
    submenu: [
      {
        label: "Mostrar u ocultar Dev Tools",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        },
      },
      {
        role: "reload",
      },
    ],
  });
}